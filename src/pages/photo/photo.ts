import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, AlertController} from 'ionic-angular';
import { AddPupil } from '../add-pupil/add-pupil';
import { Contractors } from '../contractors/contractors';
import { Pupil } from "../../models/pupil";
import { DataRequest } from "../../models/DataRequest"
import { PupilProvider }  from "../../providers/pupil-provider";
import { Offline } from "../../utility/offline";
import { SenatorialDistrictProvider } from "../../providers/senatorial-district-provider";
import { SenatorialDistrictProviderAll } from "../../providers/senatorial-district-provider-all/senatorial-district-provider-all";
import { VulnerabilityAll } from "../../providers/vulnerability-all/vulnerability-all";


import { LGAProvider } from "../../providers/lga-provider";
import { WardProvider } from "../../providers/ward-provider";
import { SchoolProvider } from "../../providers/school-provider";
import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';
import { Camera, CameraOptions } from '@ionic-native/camera';


declare var Crypt;
declare var Enumerable;
/**
 * Generated class for the Photo page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-photo',
  templateUrl: 'photo.html',
  providers: [ PupilProvider, SenatorialDistrictProvider, 
  LGAProvider, WardProvider, SchoolProvider, SecureStorage, VulnerabilityAll,
  SenatorialDistrictProviderAll ]
})
export class Photo {
  public base64Image : string = "assets/img/avatar-placeholder.png";
  public Request : DataRequest = new DataRequest()
  public Student : Pupil = new Pupil()
  public SecretKey : string = "c1JQ1igCsBohf*17%$n2#OPFRjYosl1jkCq&YhDn64L$m6eqhw-fhCiaONL9^u1!zJn?RDmY$-gDnBAbnUn^h!Q0VTm=@0oy9";
  public ClientId : string = "2TF0NTBFBN";
  public SenatorialList : Object = [];
  public LGAList : Object = [];
  public WardList : Object = [];
  public SchoolList : Object = [];
  public isDebugMode : Boolean = false;
  public StateId : number = null;
  public UserId : number = null;
  public Pupils : Pupil[] = [];
  public VulnerabilityList : Object = [];
  public Selected  = {
      DistrictId : null,
      LGAId : null,
      WardId : null,
      SchoolId : null
  }
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public loadingCtrl : LoadingController,
     public events : Events,
     public senatorialDistrictProvider : SenatorialDistrictProvider,
     public senatorialDistrictProviderAll : SenatorialDistrictProviderAll,
     public lgaProvider : LGAProvider,
     public wardProvider: WardProvider,
     public schoolProvider : SchoolProvider,
     private camera: Camera,
     public alertCtrl : AlertController,
     public secureStorage : SecureStorage,
     public vulnerabilityAll : VulnerabilityAll,
     public pupilProvider : PupilProvider) {
         this.SenatorialList = this.GetDistrict();
         this.VulnerabilityList = this.GetVulnerability();
        // this.LGAList = this.GetLGA();
         //this.WardList =  this.GetWards();
        // this.SchoolList = this.GetSchools();

         if(!this.isDebugMode) {
               // this.nativeStorage.getItem("UserDetails").then((data : any) => {
               //     var userDetails = JSON.parse(data);
               //    this.StateId = userDetails.state_id;
               //     this.Request.user_id = parseInt(userDetails.id);
               // });

               var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
                 setTimeout(() => {
                        console.log(userDetails);
                        this.StateId = userDetails.data.state_id;
                        this.Request.user_id = parseInt(userDetails.data.id);
                        this.senatorialDistrictProvider.fetch(this.StateId).then((res : any) => {
                                if(res.status) {
                                    this.SenatorialList = res.data;
                                    //Save here
                                    this.SaveDistrict(res.data);
                                }
                        });
                        this.vulnerabilityAll.fetchAll().then((res : any) => {
                            if(res.status) {
                                this.SaveVulnerability(res.data);
                            }
                        });
                 }, 2000);
               
               
               //this.secureStorage.create('UserInfoDB')
               //     .then((storage: SecureStorageObject) => {
               //         storage.get('UserInfo')
               //          .then(
               //             (data : any) =>{
               //                 var userDetails  = JSON.parse(data);
                                
               //             },
               //             (error : any) => {
               //                console.log(error);
               //                 this.alert(JSON.stringify(error));
               //             }
               //         );
               //});
         } else {
            this.StateId = 29;
            this.Request.user_id = 210;
         }

         this.senatorialDistrictProvider.fetch(this.StateId).then((res : any) => {
            if(res.status) {
                this.SenatorialList = res.data;
            }
        });

        this.vulnerabilityAll.fetchAll().then((res : any) => {
            if(res.status) {
                this.VulnerabilityList = res.data;
            }
        });
         
  }

  ionViewDidLoad() {
  
    console.log('ionViewDidLoad Photo');
  }

  public Photo () : void {
    const options : CameraOptions = {
        quality: 50, // picture quality
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options) .then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        }, (err) => {
        // console.log(err);
    });
 }

public Gallery () : void {
    this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: this.camera.DestinationType.DATA_URL
        }).then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,'+ imageData;
        }, (err) => {
        //console.log(err);
        });
}

public VulnerabilityChange () : void {

}
  private DistrictChange () : void {
      var value = localStorage.getItem("LGAProviderAll");
      if(value != "undefined" && value != "null"){
            var lgas = JSON.parse(localStorage.getItem("LGAProviderAll"));
            this.LGAList  = Enumerable
                        .From(lgas)
                        .Where((x : any ) => {
                            return x.senatorial_district_id == this.Selected.DistrictId
                        })
                        .ToArray();
            this.WardList = [];
            this.SchoolList = [];
      }
  }

  private LGAChange () : void {
      var wards = JSON.parse(localStorage.getItem("WardProviderAll"));
      this.WardList = Enumerable
                   .From(wards)
                   .Where((ward : any) => {
                         return ward.lga_id == this.Selected.LGAId
                   })
                   .ToArray();
     this.SchoolList = [];
  }

  private WardChange () : void {
        var value = localStorage.getItem("SchoolProviderAll");
        if(value != "undefined" && value != "null") {
            var schools = JSON.parse(localStorage.getItem("SchoolProviderAll"));
            this.SchoolList = Enumerable    
                            .From(schools)
                            .Where((school : any) => {
                                return school.ward_id == this.Selected.WardId;
                            })
                            .ToArray();
        }
  }

  public SchoolChange () : void {
      this.Student.school = this.Selected.SchoolId;
  }
  public Add(){
    this.navCtrl.push(AddPupil);
  }
  public Contractors(){
    this.navCtrl.push(Contractors);
  }

  public Submit () : void {
    this.Student.pupil_id = this.getRandomInt(1,99999);

    if(this.Student.surname == null || this.Student.surname.trim() == "") {
        this.alert("Please Enter Surname"); return;
    }
    
    if(this.Student.firstname == null || this.Student.firstname.trim() == "") {
        this.alert("Please Enter First Name"); return;
    }
    
    if(this.Student.othernames == null || this.Student.othernames.trim() == "") {
        this.alert("Please Enter Other Names"); return;
    }

    if(this.Student.gender == null || this.Student.gender.trim() == "") {
        this.alert("Please Select Gender"); return;
    }

    if(this.Student.dob == null || this.Student.dob.trim() == "") {
        this.alert("Please Enter Date Of Birth"); return;
    }

    if(this.Student.parentname == null || this.Student.parentname.trim() == "") {
        this.alert("Please Enter Parent Name"); return;
    }

    if(this.Student.address == null || this.Student.address.trim() == "") {
        this.alert("Please Enter Address"); return;
    }

    if(this.Student.email == null || this.Student.email.trim() == "") {
        this.alert("Please Enter Email Address"); return;
    }

    if(this.Student.vulnerability_status == null || this.Student.vulnerability_status == "") {
        this.alert("Please Select Student Vulnerability Status"); return;
    }

    if(this.Student.height == null) {
        this.alert("Please Enter Height"); return;
    }

    if(this.Student.weight == null) {
        this.alert("Please Enter Weight"); return;
    }

    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.Request.pupils = [];
    this.Request.user_id = userDetails.data.id;
    this.Request.client_id = this.ClientId
    this.Student.school = parseInt(this.Selected.SchoolId);
    this.Student.passport = this.base64Image.replace("data:image/jpeg;base64,", ""); 
    this.Request.pupils.push(this.Student);
    this.Request.pupil_count = this.Request.pupils.length;
    this.Request.request_id = this.randomString();
   
    var value = this.ClientId + "-" 
        + this.Request.pupils.length + "-" 
        + this.Request.request_id + "-"
        + this.SecretKey;

    this.Request.mac = Crypt.HASH.sha512(value).toString();
    
    console.log(JSON.stringify(this.Request));

    let loading = this.loadingCtrl.create({
      content: "Please Wait..."
    });
    
    loading.present();

    this.pupilProvider.load(this.Request).then((data : any) => {
        loading.dismiss();
        if(data.resp_code == "00") {
            this.alert("You have successfully added record");
            this.ResetData();
        } else {
            this.alert("Oops something went wrong");
        }
    }, (error : any) => {
        this.alert(error);
        loading.dismiss();
    });
  }

   public randomString() : string {
        var length = 16;
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        return result;
  }

  public getRandomInt(min, max) : number {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  private ResetData () : void {
        this.Student = new Pupil();
        this.Request = new DataRequest();
        this.Selected= {
                DistrictId : null,
                LGAId : null,
                WardId : null,
                SchoolId : null
        }
        this.base64Image = "assets/img/avatar-placeholder.png";
        this.Student.passport =  null;
        this.Student.dob =  null;
        this.Student.gender = null;
        this.Student.vulnerability_status = null;
        this.Student.head_teacher = null;
        //this.Student.health_care = null;
        this.Student.class = null;
  }

  public SaveDistrict(data : any) : void { 
       if (data != null || data != undefined){
            //Put the object into storage
            localStorage.setItem('SenatorialList', JSON.stringify(data));
       }
  }

  public SaveVulnerability (data : any) : void {
    if(data != null || data != undefined) {
        localStorage.setItem("VulnerabilityList", JSON.stringify(data));
     }
  }

  public SaveLGA(data : any) : void{
      if(data != null || data != undefined){
        localStorage.setItem("LGAList", JSON.stringify(data))
      }
  }
  public SaveWards(data : any) : void {
      if (data != null || data != undefined){
         localStorage.setItem("WardList", JSON.stringify(data))
      }
  }
  public SaveSchool(data : any) : void {
      if (data != null || data != undefined){
        localStorage.setItem("SchoolList", JSON.stringify(data));
      }
  }
  
  public GetDistrict () : any {
      var value = localStorage.getItem("SenatorialList");
      if(value != "undefined" && value != "null"){
            var list = JSON.parse(localStorage.getItem("SenatorialList"));//SenatorialDistrictAl
            return list != null ? list : [];
      } else {
          return [];
      }
  }

  public GetVulnerability () : any {
      var value = localStorage.getItem("VulnerabilityList");
      if(value != "undefined" && value != "null"){
            var list = JSON.parse(localStorage.getItem("VulnerabilityList"));
            return list != null ? list : [];
      } else {
          return [];
      }
  }
  public GetLGA() : any {
       var value = localStorage.getItem("LGAProviderAll");
       if(value != "undefined" && value != "null"){
            var list = JSON.parse(localStorage.getItem("LGAProviderAll")); //LGAList
            return list != null ? list : [];
       } else {
           return [];
       }
  }

  public GetWards () : any {
      var value = localStorage.getItem("WardProviderAll");
      if(value != "undefined" && value != "null"){
        var list = JSON.parse(localStorage.getItem("WardProviderAll")); //WardList
        return list != null ? list : [];
      } else {
          return [];
      }
  }

  public GetSchools () : any {
      var value = localStorage.getItem("SchoolProviderAll");
      if(value != "undefined" && value != "null"){
            var list = JSON.parse(localStorage.getItem("SchoolProviderAll")); //SchoolList
            return list != null ? list : [];
      } else {
            return [];
      }
  }

  private AddPupil() : void {

    this.Student.pupil_id = this.getRandomInt(1,99999);

    if(this.Selected.DistrictId == null || parseInt(this.Selected.DistrictId) == NaN) {
        this.alert("Please select senatorial district"); return ;
    }

    if(this.Selected.LGAId == null || parseInt(this.Selected.LGAId) == NaN) {
        this.alert("Please select LGA"); return;
    }

    if(this.Selected.WardId == null || parseInt(this.Selected.WardId) == NaN) {
        this.alert("Please select Ward"); return;
    }

    if(this.Selected.SchoolId == null || parseInt(this.Selected.SchoolId) == NaN) {
        this.alert("Please select School"); return;
    }

    if(this.Student.surname == null || this.Student.surname.trim() == "") {
        this.alert("Please enter surname"); return;
    }
    
    if(this.Student.firstname == null || this.Student.firstname.trim() == "") {
        this.alert("Please enter first name"); return;
    }
    
    if(this.Student.othernames == null || this.Student.othernames.trim() == "") {
        this.alert("Please enter other names"); return;
    }

    if(this.Student.dob == null || this.Student.dob.trim() == "") {
        this.alert("Please enter date of birth"); return;
    }
    
    if(this.Student.gender == null || this.Student.gender.trim() == "") {
        this.alert("Please select gender"); return;
    }

    if(this.Student.parentname == null || this.Student.parentname.trim() == "") {
        this.alert("Please enter parent name"); return;
    }
    
    if(this.Student.vulnerability_status == null || this.Student.vulnerability_status.trim() == "") {
        this.alert("Please select vulnerability status"); return;
    }

    if(this.Student.phone == null || this.Student.phone.trim() == "") {
        this.alert("Please enter phone number"); return;
    }

    if(this.Student.email == null || this.Student.email.trim() == "") {
        this.alert("Please enter email address"); return;
    }

    if(this.Student.address == null || this.Student.address.trim() == "") {
        this.alert("Please enter address"); return;
    }

    if(this.Student.height == null) {
        this.alert("Please enter height"); return;
    }

    if(this.Student.weight == null) {
        this.alert("Please enter weight"); return;
    }

    let ionAlert = this.alertCtrl.create({
        title: "Confirm",
        message : "Are you sure you want to add to list?",
        buttons : [
            {
                text : "Cancel",
                role : "cancel",
                handler : () => {}
            },
            {
                text : "Continue",
                handler : () => {
                    this.Student.school = parseInt(this.Selected.SchoolId);
                    //Script out the data:image/jpeg;base64,
                    this.Student.passport = this.base64Image.replace("data:image/jpeg;base64,", "");

                    var pupils : Pupil[] = JSON.parse(localStorage.getItem("Pupils"));
                    if (pupils == null) {
                        this.Pupils.push(this.Student);
                        localStorage.setItem("Pupils", JSON.stringify(this.Pupils));
                    } else {
                        pupils.push(this.Student);
                        localStorage.setItem("Pupils", JSON.stringify(pupils));
                    }

                    this.alert("You\'ve successfully added " + this.Student.surname + " " + this.Student.firstname  + " to list");
                    this.ResetData();
                }
            }
        ]
    });  
    ionAlert.present();  
  }

  private alert(message : string) : void {
      let ionAlert = this.alertCtrl.create({
            title : "Alert",
            message : message,
            buttons : [
                {
                    text : "OK",
                    role : "cancel",
                    handler : () => {}
                }
            ]
      });
      ionAlert.present();
  }

  
}
