import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Pupil } from "../../models/pupil";
import { DataRequest } from "../../models/DataRequest";
import { PupilProvider }  from "../../providers/pupil-provider";
import { CatererModel } from "../../models/CatererModel";
import { CatererProvider } from "../../providers/caterer/caterer";
import { FarmerProvider } from "../../providers/farmer/farmer";

declare var Crypt;
declare var Enumerable;
/**
 * Generated class for the Listing page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-listing',
  templateUrl: 'listing.html',
  providers : [PupilProvider, CatererProvider, FarmerProvider]
})
export class Listing {
  private Pupils : Pupil[] = [];
  private Caterers : CatererModel[] = [];
  private Farmers : CatererModel[] = [];
  public Request : DataRequest = new DataRequest()
  public SecretKey : string = "c1JQ1igCsBohf*17%$n2#OPFRjYosl1jkCq&YhDn64L$m6eqhw-fhCiaONL9^u1!zJn?RDmY$-gDnBAbnUn^h!Q0VTm=@0oy9";
  public ClientId : string = "2TF0NTBFBN";
  
  constructor(
    public navCtrl: NavController, 
    public pupilProvider : PupilProvider,
    public loadingCtrl : LoadingController,
    public toastCtrl : ToastController,
    public _catererProvider : CatererProvider,
    public _farmerProvider : FarmerProvider,
    public navParams: NavParams) {
        this.Pupils = JSON.parse(localStorage.getItem("Pupils"));
        this.Caterers = JSON.parse(localStorage.getItem("Caterers"));
        this.Farmers = JSON.parse(localStorage.getItem("Farmers"));
  }

  ionViewDidLoad() {
    this.Pupils = JSON.parse(localStorage.getItem("Pupils"));
    this.Caterers = JSON.parse(localStorage.getItem("Caterers"));
    this.Farmers = JSON.parse(localStorage.getItem("Farmers"));

    setInterval(() => {
          this.Pupils = JSON.parse(localStorage.getItem("Pupils"));
          this.Caterers = JSON.parse(localStorage.getItem("Caterers"));
          this.Farmers = JSON.parse(localStorage.getItem("Farmers"));

          this.UploadData();
          this.UploadCatererData();
          this.UploadFarmerData();
        }, 10000);
  }

  ionViewWillEnter () {
    this.Pupils = JSON.parse(localStorage.getItem("Pupils"));
    this.Caterers = JSON.parse(localStorage.getItem("Caterers"));
    this.Farmers = JSON.parse(localStorage.getItem("Farmers"));
  }

  private UploadData () : void {
    if(this.Pupils != null)
      if(this.Pupils.length >= 1)  
        var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
        if(userDetails != null)
        {
            this.Request.pupils = [];
            this.Request.user_id = userDetails.data.id;
            this.Request.client_id = this.ClientId;
            this.Request.pupils = this.Pupils;
            this.Request.pupil_count = this.Request.pupils.length;
            this.Request.request_id = this.randomString();
          
            var value = this.ClientId + "-" 
                + this.Request.pupils.length + "-" 
                + this.Request.request_id + "-"
                + this.SecretKey;

            this.Request.mac = Crypt.HASH.sha512(value).toString();

            let loading = this.loadingCtrl.create({
              content: "Please Wait..."
            });

            this.pupilProvider.load(this.Request).then((data : any) => {
                loading.dismiss();
                if(data.resp_code == "00") {
                  let toast = this.toastCtrl.create({
                      message: 'Data sync successful',
                      duration: 3000
                    });
                    toast.present();
                    this.ResetPupils();
                } else {
                    let toast = this.toastCtrl.create({
                      message: 'Unable to sync data',
                      duration: 3000
                    });
                    toast.present();
                }
            }, (error : any) => {
                let toast = this.toastCtrl.create({
                      message: 'Unable to sync data',
                      duration: 3000
                    });
                    toast.present();
                loading.dismiss();
            });
        }
        
  }

  private UploadCatererData () : void {
    if(this.Caterers != null)
      if(this.Caterers.length >= 1)  
        var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
        if(userDetails != null)
        {
            this.Request.records = [];
            this.Request.user_id = userDetails.data.id;
            this.Request.client_id = this.ClientId;
            this.Request.records = this.Caterers;
            this.Request.record_count = this.Request.records.length;
            this.Request.request_id = this.randomString();
          
            var value = this.ClientId + "-" 
                + this.Request.records.length + "-" 
                + this.Request.request_id + "-"
                + this.SecretKey;

            this.Request.mac = Crypt.HASH.sha512(value).toString();

            let loading = this.loadingCtrl.create({
              content: "Please Wait..."
            });

            this._catererProvider.load(this.Request).then((data : any) => {
                loading.dismiss();
                if(data.resp_code == "00") {
                  let toast = this.toastCtrl.create({
                      message: 'Data sync successful',
                      duration: 3000
                    });
                    toast.present();
                    this.ResetCaterers();
                } else {
                    let toast = this.toastCtrl.create({
                      message: 'Unable to sync data',
                      duration: 3000
                    });
                    toast.present();
                }
            }, (error : any) => {
                let toast = this.toastCtrl.create({
                      message: 'Unable to sync data',
                      duration: 3000
                    });
                    toast.present();
                loading.dismiss();
            });
        }
        
  }

 private UploadFarmerData () : void {
    if(this.Farmers != null)
      if(this.Farmers.length >= 1)  
        var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
        if(userDetails != null)
        {
            this.Request.records = [];
            this.Request.user_id = userDetails.data.id;
            this.Request.client_id = this.ClientId;
            this.Request.records = this.Farmers;
            this.Request.record_count = this.Request.records.length;
            this.Request.request_id = this.randomString();
          
            var value = this.ClientId + "-" 
                + this.Request.records.length + "-" 
                + this.Request.request_id + "-"
                + this.SecretKey;

            this.Request.mac = Crypt.HASH.sha512(value).toString();

            let loading = this.loadingCtrl.create({
              content: "Please Wait..."
            });

          // loading.present();

            this._farmerProvider.load(this.Request).then((data : any) => {
                loading.dismiss();
                if(data.resp_code == "00") {
                  let toast = this.toastCtrl.create({
                      message: 'Data sync successful',
                      duration: 3000
                    });
                    toast.present();
                    this.ResetFarmers();
                } else {
                    let toast = this.toastCtrl.create({
                      message: 'Unable to sync data',
                      duration: 3000
                    });
                    toast.present();
                }
            }, (error : any) => {
                let toast = this.toastCtrl.create({
                      message: 'Unable to sync data',
                      duration: 3000
                    });
                    toast.present();
                loading.dismiss();
            });
        }
        
  }
   public randomString() : string {
     var length = 16;
     var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var result = '';
      for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
      return result;
  }
  public ResetPupils () : void {
      localStorage.setItem("Pupils", null);
      this.Pupils = [];
  }

  private ResetCaterers () : void {
      localStorage.setItem("Caterers", null);
      this.Caterers = [];
  }

  private ResetFarmers () : void {
      localStorage.setItem("Farmers", null);
      this.Farmers = [];
  }

  public convert (image : string) : string {
      return "data:image/jpeg;base64," + image ;
  }
}
