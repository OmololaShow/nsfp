import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, AlertController} from 'ionic-angular';
import { AddPupil } from '../add-pupil/add-pupil';
import { Contractors } from '../contractors/contractors';
import { CatererModel } from "../../models/CatererModel";
import { DataRequest } from "../../models/DataRequest"
import { Offline } from "../../utility/offline";
import { SenatorialDistrictProvider } from "../../providers/senatorial-district-provider";
import { SenatorialDistrictProviderAll } from "../../providers/senatorial-district-provider-all/senatorial-district-provider-all";
import { VulnerabilityAll } from "../../providers/vulnerability-all/vulnerability-all";
import { FarmerProvider } from "../../providers/farmer/farmer";
import { BankProvider } from "../../providers/bank/bank";

import { LGAProvider } from "../../providers/lga-provider";
import { WardProvider } from "../../providers/ward-provider";
import { SchoolProvider } from "../../providers/school-provider";
import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { BankModel } from "../../models/BankModel";
import { BankResponseModel } from "../../models/BankResponseModel";

declare var Crypt;
declare var Enumerable;

/**
 * Generated class for the CatererPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-farmer',
  templateUrl: 'farmer.html',
  providers: [BankProvider, FarmerProvider, SenatorialDistrictProvider, 
  LGAProvider, WardProvider, SchoolProvider, SecureStorage, VulnerabilityAll,
  SenatorialDistrictProviderAll ]
})

export class FarmerPage {
public base64Image : string = "assets/img/farmer1.png";
  public Request : DataRequest = new DataRequest()
  public Caterer : CatererModel = new CatererModel()
  public SecretKey : string = "c1JQ1igCsBohf*17%$n2#OPFRjYosl1jkCq&YhDn64L$m6eqhw-fhCiaONL9^u1!zJn?RDmY$-gDnBAbnUn^h!Q0VTm=@0oy9";
  public ClientId : string = "2TF0NTBFBN";
  public SenatorialList : Object = [];
  public LGAList : Object = [];
  public WardList : Object = [];
  public SchoolList : Object = [];
  public isDebugMode : Boolean = false;
  public StateId : number = null;
  public UserId : number = null;
  public Caterers : CatererModel[] = [];
  public VulnerabilityList : Object = [];
  public Banks : BankModel[] = [];
  public Selected  = {
      DistrictId : null,
      LGAId : null,
      WardId : null,
      SchoolId : null
  }
  public BankId : string;
  public PupilCount : string;
  public EmployeeCount : string;
  public LandSize : string
  public LinkedCook : string
  public PreviousIncome : string
  public CurrentIncome : string

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public loadingCtrl : LoadingController,
     public events : Events,
     public senatorialDistrictProvider : SenatorialDistrictProvider,
     public senatorialDistrictProviderAll : SenatorialDistrictProviderAll,
     public lgaProvider : LGAProvider,
     public wardProvider: WardProvider,
     public schoolProvider : SchoolProvider,
     private camera: Camera,
     public alertCtrl : AlertController,
     public secureStorage : SecureStorage,
     public vulnerabilityAll : VulnerabilityAll,
     public bankProvider : BankProvider,
     public farmerProvider : FarmerProvider) {
         this.SenatorialList = this.GetDistrict();
         this.VulnerabilityList = this.GetVulnerability();
         this.Banks = this.GetLocalBanks();
         if(!this.isDebugMode) {
               var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
                 setTimeout(() => {
                        console.log(userDetails);
                        this.StateId = userDetails.data.state_id;
                        this.Request.user_id = parseInt(userDetails.data.id);
                        this.senatorialDistrictProvider.fetch(this.StateId).then((res : any) => {
                                if(res.status) {
                                    this.SenatorialList = res.data;
                                    //Save here
                                    this.SaveDistrict(res.data);
                                }
                        });
                        this.vulnerabilityAll.fetchAll().then((res : any) => {
                            if(res.status) {
                                this.SaveVulnerability(res.data);
                            }
                        });
                 }, 2000);
         } else {
            this.StateId = 29;
            this.Request.user_id = 210;
         }

         this.senatorialDistrictProvider.fetch(this.StateId).then((res : any) => {
            if(res.status) {
                this.SenatorialList = res.data;
            }
        });

        this.vulnerabilityAll.fetchAll().then((res : any) => {
            if(res.status) {
                this.VulnerabilityList = res.data;
            }
        });

        this.bankProvider.fetchAll().then((res : BankResponseModel) => {
            if(res.status){
                this.Banks = res.data;
                this.SetLocalBanks(res.data);
            }
        });
  }

  ionViewDidLoad() {
  
    console.log('ionViewDidLoad Photo');
  }

  public Photo () : void {
    const options : CameraOptions = {
        quality: 50, // picture quality
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options) .then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        }, (err) => {
        // console.log(err);
    });
 }

public Gallery () : void {
    this.camera.getPicture({
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: this.camera.DestinationType.DATA_URL
        }).then((imageData) => {
        this.base64Image = 'data:image/jpeg;base64,'+ imageData;
        }, (err) => {
        //console.log(err);
        });
}

public VulnerabilityChange () : void {

}
  private DistrictChange () : void {
      var value = localStorage.getItem("LGAProviderAll");
      if(value != "undefined"){
            var lgas = JSON.parse(localStorage.getItem("LGAProviderAll"));
            this.LGAList  = Enumerable
                        .From(lgas)
                        .Where((x : any ) => {
                            return x.senatorial_district_id == this.Selected.DistrictId
                        })
                        .ToArray();
            this.WardList = [];
            this.SchoolList = [];
      }
  }

  private LGAChange () : void {
      var value = localStorage.getItem("WardProviderAll");
      if(value != "undefined"){
            var wards = JSON.parse(localStorage.getItem("WardProviderAll"));
            this.WardList = Enumerable
                        .From(wards)
                        .Where((ward : any) => {
                                return ward.lga_id == this.Selected.LGAId
                        })
                        .ToArray();
            this.SchoolList = [];
      }
  }

  private WardChange () : void {
        var value = localStorage.getItem("SchoolProviderAll");
        if(value != "undefined"){
            var schools = JSON.parse(localStorage.getItem("SchoolProviderAll"));
            this.SchoolList = Enumerable    
                            .From(schools)
                            .Where((school : any) => {
                                return school.ward_id == this.Selected.WardId;
                            })
                            .ToArray();
        }
  }

  public SchoolChange () : void {
      this.Caterer.school_id = this.Selected.SchoolId;
  }
  public Add(){
    this.navCtrl.push(AddPupil);
  }
  public Contractors(){
    this.navCtrl.push(Contractors);
  }

  public Submit () : void {
    this.Caterer.id = this.getRandomInt(1,99999).toString();

    if(this.Caterer.fullname == null || this.Caterer.fullname.trim() == "") {
        this.alert("Please Enter Surname"); return;
    }

    if(this.Caterer.dob == null || this.Caterer.dob.trim() == "") {
        this.alert("Please Enter Date Of Birth"); return;
    }

    if(this.Caterer.address == null || this.Caterer.address.trim() == "") {
        this.alert("Please Enter Address"); return;
    }

    if(this.Caterer.email == null || this.Caterer.email.trim() == "") {
        this.alert("Please Enter Email Address"); return;
    }

    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
    this.Request.records = [];
    this.Request.user_id = userDetails.data.id;
    this.Request.client_id = this.ClientId
    this.Caterer.school_id = parseInt(this.Selected.SchoolId);
    this.Caterer.passport = this.base64Image.replace("data:image/jpeg;base64,", "");;
    this.Request.records.push(this.Caterer);

    this.Request.request_id = this.randomString();
   
    var value = this.ClientId + "-" 
        + this.Request.records.length + "-" 
        + this.Request.request_id + "-"
        + this.SecretKey;

    this.Request.mac = Crypt.HASH.sha512(value).toString();
    
    console.log(JSON.stringify(this.Request));

    let loading = this.loadingCtrl.create({
      content: "Please Wait..."
    });

    loading.present();

    this.farmerProvider.load(this.Request).then((data : any) => {
        loading.dismiss();
        if(data.resp_code == "00") {
            this.alert("You have successfully added record");
            this.ResetData();
        } else {
            this.alert("Oops something went wrong");
        }
    }, (error : any) => {
        this.alert(error);
        loading.dismiss();
    });
  }

   public randomString() : string {
     var length = 16;
     var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      var result = '';
      for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
      return result;
  }

  public getRandomInt(min, max) : number {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  private ResetData () : void {
        this.Caterer = new CatererModel();
        this.Request = new DataRequest();
        this.Selected= {
                DistrictId : null,
                LGAId : null,
                WardId : null,
                SchoolId : null
        }
        this.base64Image = "assets/img/avatar-placeholder.png";
        this.Caterer.passport =  null;
        this.Caterer.dob = null;
        this.Caterer.maritalstatus = null;
        this.BankId = null;
        this.PupilCount = null;
        this.EmployeeCount = null;
        this.LandSize = null;
        this.LinkedCook = null;
        this.PreviousIncome = null;
        this.CurrentIncome = null;
  }

  public SaveDistrict(data : any) : void { 
       if (data != null || data != undefined){
        localStorage.setItem('SenatorialList', JSON.stringify(data));
       }
  }

  public SaveVulnerability (data : any) : void {
    if(data != null || data != undefined) {
        localStorage.setItem("VulnerabilityList", JSON.stringify(data));
    }
  }

  public SaveLGA(data : any) : void{
      if (data != null || data != undefined){
        localStorage.setItem("LGAList", JSON.stringify(data))
    }
  }
  public SaveWards(data : any) : void {
      if (data != null || data != undefined){
         localStorage.setItem("WardList", JSON.stringify(data))
      }
  }
  public SaveSchool(data : any) : void {
      if (data != null || data != undefined){
        localStorage.setItem("SchoolList", JSON.stringify(data));
      }
  }
  public GetDistrict () : any {
      var value = localStorage.getItem("SenatorialList");
      if(value != "undefined"){
            var list = JSON.parse(localStorage.getItem("SenatorialList"));//SenatorialDistrictAl
            if(list == undefined){
                return [];
            }
            return list != null ? list : [];
     } else {
         return [];
     }
  }

  public GetVulnerability () : any {
      var value = localStorage.getItem("VulnerabilityList");
      if(value != "undefined"){
            var list = JSON.parse(localStorage.getItem("VulnerabilityList"));
            console.log(list);
            if(list == undefined){
                return [];
            }
            return list != null ? list : [];
      } else {
          return [];
      }
  }

  public GetLocalBanks () : any {
      var value = localStorage.getItem("BankList");
      if(value != "undefined"){
            var list = JSON.parse(localStorage.getItem("BankList"));
            if(list == undefined){
                return [];
            }
            return list != null ? list : [];
     } else {
         return [];
     }
  }


  public SetLocalBanks(data : BankModel[]) : void { 
       if (data != null || data != undefined){
        localStorage.setItem('BankList', JSON.stringify(data));
       }
  }

  public GetLGA() : any {
      var value = localStorage.getItem("LGAProviderAll");
      if(value != "undefined") {
            var list = JSON.parse(localStorage.getItem("LGAProviderAll")); //LGAList
            return list != null ? list : [];
      } else {
          return [];
      }
  }

  public GetWards () : any {
      var value = localStorage.getItem("WardProviderAll");
      if(value != "undefined") {
        var list = JSON.parse(localStorage.getItem("WardProviderAll")); //WardList
        return list != null ? list : [];
      } else {
          return [];
      }
  }

  public GetSchools () : any {
      var value = localStorage.getItem("SchoolProviderAll");
      if(value != "undefined"){
        var list = JSON.parse(localStorage.getItem("SchoolProviderAll")); //SchoolList
        return list != null ? list : [];
      } else {
          return [];
      }
  }

  private AddCaterer() : void {

    this.Caterer.id = this.getRandomInt(1,99999).toString();

    if(this.Selected.DistrictId == null || parseInt(this.Selected.DistrictId) == NaN) {
        this.alert("Please select senatorial district"); return ;
    }

    if(this.Selected.LGAId == null || parseInt(this.Selected.LGAId) == NaN) {
        this.alert("Please select LGA"); return;
    }

    if(this.Selected.WardId == null || parseInt(this.Selected.WardId) == NaN) {
        this.alert("Please select Ward"); return;
    }

    if(this.Selected.SchoolId == null || parseInt(this.Selected.SchoolId) == NaN) {
        this.alert("Please select School"); return;
    }

    if(this.Caterer.fullname == null || this.Caterer.fullname.trim() == "") {
        this.alert("Please Enter Full Name"); return;
    }
    
    if(this.Caterer.address == null || this.Caterer.address.trim() == "") {
        this.alert("Please Enter Address"); return;
    }
    
    if(this.Caterer.phone == null || this.Caterer.phone.trim() == "") {
        this.alert("Please enter phone number"); return;
    }

    if(this.Caterer.email == null || this.Caterer.email.trim() == "") {
        this.alert("Please enter email address"); return;
    }

    if(this.Caterer.dob == null || this.Caterer.dob.trim() == "") {
        this.alert("Please Enter Date Of Birth"); return;
    }

    if(this.Caterer.maritalstatus == null || this.Caterer.maritalstatus.trim() == "") {
        this.alert("Please select marital status"); return;
    }

    if(this.Caterer.accno == null || this.Caterer.accno.trim() == "") {
        this.alert("Please enter account number"); return;
    }

    if(this.Caterer.accname == null || this.Caterer.accname.trim() == "") {
        this.alert("Please enter account name"); return;
    }

    if(this.BankId == null || parseInt(this.BankId) == NaN) {
        this.alert("Please select bank"); return ;
    }

    if(this.Caterer.bvn == null || this.Caterer.bvn.trim() == "") {
        this.alert("Please enter bvn number"); return;
    }

    if(this.Caterer.pupilgroup == null || this.Caterer.pupilgroup.trim() == "") {
        this.alert("Please enter pupil group"); return;
    }

    if(this.PupilCount == null || parseInt(this.PupilCount) == NaN) {
        this.alert("Please enter pupil count"); return ;
    }

    if(this.EmployeeCount == null || parseInt(this.EmployeeCount) == NaN) {
        this.alert("Please enter employee count"); return ;
    }

    if(this.Caterer.crop_farm_occupation == null || this.Caterer.crop_farm_occupation.trim() == "") {
        this.alert("Please enter farm occupation"); return;
    }

    if(this.Caterer.landtype == null || this.Caterer.landtype.trim() == "") {
        this.alert("Please enter land type"); return;
    }

    if(this.LandSize == null || parseInt(this.LandSize) == NaN) {
        this.alert("Please enter land size"); return ;
    }

    if(this.LinkedCook == null || parseInt(this.LinkedCook) == NaN) {
        this.alert("Please enter linked cook"); return ;
    }

    if(this.PreviousIncome == null || parseInt(this.PreviousIncome) == NaN) {
        this.alert("Please enter previous income"); return ;
    }

    if(this.CurrentIncome == null || parseInt(this.CurrentIncome) == NaN) {
        this.alert("Please enter current income"); return ;
    }

    if(this.Caterer.aggregatordetails == null || this.Caterer.aggregatordetails.trim() == "") {
        this.alert("Please enter aggregator details"); return;
    }

    let ionAlert = this.alertCtrl.create({
        title: "Confirm",
        message : "Are you sure you want to add to list?",
        buttons : [
            {
                text : "Cancel",
                role : "cancel",
                handler : () => {}
            },
            {
                text : "Continue",
                handler : () => {
                    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
                    this.Caterer.school_id = parseInt(this.Selected.SchoolId);
                    this.Caterer.passport = this.base64Image.replace("data:image/jpeg;base64,", "");;
                    this.Caterer.bank_id = parseInt(this.BankId);
                    this.Caterer.senatorial_district_id = parseInt(this.Selected.DistrictId);
                    this.Caterer.lga_id = parseInt(this.Selected.LGAId);
                    this.Caterer.ward_id = parseInt(this.Selected.WardId);
                    this.Caterer.state_id = userDetails.data.state_id;
                    this.Caterer.pupilcount = parseInt(this.PupilCount);
                    this.Caterer.employee_count = parseInt(this.EmployeeCount);
                    this.Caterer.linkedcooks_count = parseInt(this.LinkedCook);
                    this.Caterer.landsize = parseInt(this.LandSize);
                    this.Caterer.previousincome = parseInt(this.PreviousIncome);
                    this.Caterer.currentincome = parseInt(this.CurrentIncome);
                    
                    var value = localStorage.getItem("Farmers");

                    if(value != "undefined" && value != "null" && value != null && value != "") 
                    {
                        var caterers : CatererModel[] = JSON.parse(value);
                        caterers.push(this.Caterer);
                        localStorage.setItem("Farmers", JSON.stringify(caterers));
                    } else {
                        var caterers : CatererModel[] = [];
                        caterers.push(this.Caterer);
                        localStorage.setItem("Farmers", JSON.stringify(caterers));
                    }
                    
                    this.alert("You\'ve successfully added " + this.Caterer.fullname + " " + this.Caterer.fullname  + " to list");
                    this.ResetData();
                }
            }
        ]
    });  
    ionAlert.present();  
  }

  private alert(message : string) : void {
      let ionAlert = this.alertCtrl.create({
            title : "Alert",
            message : message,
            buttons : [
                {
                    text : "OK",
                    role : "cancel",
                    handler : () => {}
                }
            ]
      });
      ionAlert.present();
  }

}
