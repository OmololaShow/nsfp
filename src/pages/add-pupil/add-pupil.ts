import { Component } from '@angular/core';
import { NavController, NavParams, NavOptions} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
* Generated class for the AddPupil page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
//@IonicPage()
@Component({
 selector: 'page-add-pupil',
 templateUrl: 'add-pupil.html',
})
export class AddPupil {
 public base64Image : string;
 constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera) {

 }
 
 public AddPhotoToPupilDetails () : void {
   this.navCtrl.pop(this.base64Image);
 }
 Photo() {
   const options : CameraOptions = {
     quality: 50, // picture quality
     destinationType: this.camera.DestinationType.DATA_URL,
     encodingType: this.camera.EncodingType.JPEG,
     mediaType: this.camera.MediaType.PICTURE
   }
   this.camera.getPicture(options) .then((imageData) => {
       this.base64Image = "data:image/jpeg;base64," + imageData;
     }, (err) => {
      // console.log(err);
     });
 }

Gallery(){
  this.camera.getPicture({
     sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
     destinationType: this.camera.DestinationType.DATA_URL
    }).then((imageData) => {
      this.base64Image = 'data:image/jpeg;base64,'+ imageData;
     }, (err) => {
      //console.log(err);
    });
}
 ionViewDidLoad() {
   console.log('ionViewDidLoad AddPupil');
 }
}
