import { Component } from '@angular/core';

//import { AboutPage } from '../about/about';
//import { ContactPage } from '../contact/contact';
//import { HomePage } from '../home/home';
import { Photo } from '../photo/photo';
import { Listing } from '../listing/listing';
import { Settings } from '../settings/settings';
import { CatererPage } from "../caterer/caterer";
import { FarmerPage } from "../farmer/farmer";
import { DashboardPage } from "../dashboard/dashboard";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  pupilRoot = Photo;
  listingRoot = Listing;
  catererRoot = CatererPage;
  farmerRoot = FarmerPage;

  dashboardRoot = DashboardPage;

  constructor() {
    this.pupilRoot = Photo;
    this.listingRoot = Listing;
    this.catererRoot = CatererPage;
    this.farmerRoot = FarmerPage;
    this.dashboardRoot = DashboardPage;
  }
}
