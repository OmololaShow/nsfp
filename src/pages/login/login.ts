import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Register } from '../register/register';
import { LoginProvider } from "../../providers/login-provider"
import { LoginModel } from "../../models/loginmodel";
import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
//@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [ LoginProvider, SecureStorage]
})
export class Login {
  public LoginInputModel : LoginModel = new LoginModel();
  public isDebugMode : Boolean = false;
  constructor(public navCtrl: NavController, public alertCtrl : AlertController, public navParams: NavParams, public loginProvider : LoginProvider, private secureStorage : SecureStorage, private loadingCtrl : LoadingController) {
    this.LoginInputModel.password = "";

    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
    if(userDetails != null) {
        this.LoginInputModel.email = userDetails.data.email;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  public LogIn () : void{

    try{
      if(localStorage.getItem("LGAProviderAll").trim() == "" 
        || localStorage.getItem("LGAProviderAll") == null
        || localStorage.getItem("SchoolProviderAll") == null
        || localStorage.getItem("SchoolProviderAll").trim() == ""
        || localStorage.getItem("SenatorialDistrictAll") == null
        || localStorage.getItem("SenatorialDistrictAll").trim() == ""
        || localStorage.getItem("WardProviderAll") == null
        || localStorage.getItem("WardProviderAll").trim() == "") {
            this.alert("Data Synchronization in progress.");
          return;
        } 
    } catch(e) {
        this.alert("Data Synchronization in progress.");
        return;
    }

    if(this.LoginInputModel.email == null || this.LoginInputModel.email.trim() == "") {
          this.alert("Please Enter Email Address");
          return;
    }
    if(this.LoginInputModel.password == null || this.LoginInputModel.password.trim() == "") {
          this.alert("Please Enter Password");
          return;
    }

    var userDetails = JSON.parse(localStorage.getItem("UserDetails"));

    if(userDetails != null && userDetails.data.email == this.LoginInputModel.email) {
        if(userDetails.data.password != this.LoginInputModel.password) {
            this.alert("Password is incorrect for offline login");
        } else {
          this.navCtrl.setRoot(TabsPage);
        }
          
    } else {
        var loading = this.loadingCtrl.create({
            content: "Signing In"
        });

        loading.present();

          this.loginProvider.login(this.LoginInputModel).then((success : any) => {
              if(success.status == true) {
                    if(!this.isDebugMode){
                        // this.nativeStorage.setItem('UserDetails', JSON.stringify(success))
                        // .then(
                        //   () => {
                        //     console.log('Stored item!');
                        //     this.navCtrl.setRoot(TabsPage);
                        //   },error => console.error('Error storing item', error));
                        success.data.password = this.LoginInputModel.password;
                        localStorage.setItem("UserDetails", JSON.stringify(success));
                        setTimeout(() => {
                            loading.dismiss();
                            this.navCtrl.setRoot(TabsPage);
                        }, 2000);
                      
                      //this.secureStorage.create('UserInfoDB')
                      //  .then((storage: SecureStorageObject) => {
                      //      storage.set('UserInfo', JSON.stringify(success))
                      //       .then((data : any) =>{ }, (error : any) => { console.log(error); this.alert(JSON.stringify(error));});
                      //  });
                            
                    } else {
                      this.navCtrl.setRoot(TabsPage);
                    }
                  } else{    
                    loading.dismiss();
                    this.alert("Unable to login, invalid credentials");
                  }
          }, (error : any) => {
                loading.dismiss();
                this.alert("Something went wrong");
          });
        } 
  }

  public Register(){
    this.navCtrl.push(Register);
  }

   private alert(message : string) : void {
      let ionAlert = this.alertCtrl.create({
            title : "Alert",
            message : message,
            buttons : [
                {
                    text : "OK",
                    role : "cancel",
                    handler : () => {}
                }
            ]
      });
      ionAlert.present();
  }
}
