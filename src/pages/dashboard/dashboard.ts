import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { DashboardProvider } from "../../providers/dashboard/dashboard";
import { Login } from "../login/login";

/**
 * Generated class for the DashboardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
  providers: [ DashboardProvider ]
})
export class DashboardPage {

  private UserDetails  = {
      FirstName : "",
      LastName : "",
      State : "",
      Email : "",
      PupilCount: 0,
      FarmerCount: 0,
      CatererCount: 0
  }
  constructor(public navCtrl: NavController, public app : App, public navParams: NavParams, public _dashboardProvider : DashboardProvider) {
      var userDetails = JSON.parse(localStorage.getItem("UserDetails"));
      this.UserDetails.FirstName = userDetails.data.firstname ;
      this.UserDetails.LastName = userDetails.data.lastname;
      this.UserDetails.State = userDetails.data.state_name;
      this.UserDetails.Email = userDetails.data.email;

      _dashboardProvider.fetchAll(userDetails.data.id).then((res : any) => {
          if(res != undefined && res != null) {
              this.UserDetails.PupilCount = res.data.pupil_count;
              this.UserDetails.FarmerCount = res.data.farmer_count;
              this.UserDetails.CatererCount = res.data.caterer_count;
          }
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  private LogOut () : void {
     //this.navCtrl.root
     this.app.getRootNav().setRoot(Login);
  }
}
