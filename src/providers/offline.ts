import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the PupilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class OfflineProvider {
 private data : any;
  constructor(public http: Http) { }
  
  public load() {
    return new Promise(resolve => {
      this.http.get("https://www.nhgsfp.gov.ng/natfeed-test/index.php/api_connect/schools_list/850")
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        }, error => {
           resolve("No response");
        });
    });
  }
}
