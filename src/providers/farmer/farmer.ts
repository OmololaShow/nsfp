import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from "../../utility/Config";
/*
  Generated class for the CatererProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FarmerProvider {
  private data : any;
  constructor(public http: Http) {
    console.log('Hello FarmerProvider Provider');
  }

   public load(DataRequest : any) {
    return new Promise(resolve => {
      var body = JSON.stringify(DataRequest);
    
      let headers = new Headers(
        { 'Content-Type': 'application/json',
          'Accept' : 'application/json' 
        }
      );
      let options = new RequestOptions({ headers: headers });
     
      this.http.post(Config.BASE_URL + "/api_connect/add_farmer", body)
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        }, error => {
           resolve("No response");
        });
    });
  }
}
