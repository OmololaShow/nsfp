import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from "../utility/Config";
/*
  Generated class for the PupilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SenatorialDistrictProvider {
 private data : any;
  constructor(public http: Http) { }
  
  public fetch(id : number) {
    return new Promise(resolve => {
      this.http.get(Config.BASE_URL + "/api_connect/senatorial_district_list/" + id)
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        }, error => {
           resolve(error);
        });
    });
  }
}
