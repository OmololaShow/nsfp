import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from "../../utility/Config";
/*
  Generated class for the SenatorialDistricProviderAllProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class WardProviderAll {
  private data : any;
  constructor(public http: Http) {
    console.log('Hello SenatorialDistricProviderAllProvider Provider');
  }

   public fetchAll() {
    return new Promise(resolve => {
      this.http.get(Config.BASE_URL + "/api_connect/wards_list/all")
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        }, error => {
           resolve(error);
        });
    });
  }
}
