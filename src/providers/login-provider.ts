import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from "../utility/Config";
/*
  Generated class for the PupilProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LoginProvider {
 private data : any;
  constructor(public http: Http) { }
  
  public login(DataRequest : any) {
    return new Promise(resolve => {
      var body = JSON.stringify(DataRequest);
    
      let headers = new Headers(
        { 
            'Content-Type': 'application/json'
        }
      );
      let options = new RequestOptions({ headers: headers });
     
      this.http.post(Config.BASE_URL + "/api_connect/do_login", body)
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        }, error => {
           resolve(error);
        });
    });
  }
}
