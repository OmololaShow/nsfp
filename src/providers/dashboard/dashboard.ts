import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Config } from "../../utility/Config";
/*
  Generated class for the LgaProviderAllProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DashboardProvider {
  private data : any;
  constructor(public http: Http) {
    console.log('Hello LgaProviderAllProvider Provider');
  }
  public fetchAll(id : string) {
    return new Promise(resolve => { 
      this.http.get(Config.BASE_URL + "/api_connect/get_dashboard_data/" + id)
        .map(res => res.json())
        .subscribe(data => {
           this.data = data;
           resolve(this.data);
        });
    });
  }
}
