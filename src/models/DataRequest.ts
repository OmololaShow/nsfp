import { Pupil } from "../models/pupil";
import { CatererModel } from "../models/CatererModel";
export class DataRequest {
    public user_id : number
    public mac : string 
    public client_id : string
    public request_id : string
    public pupil_count : number
    public pupils : Pupil[]
    public record_count : number
    public records : CatererModel[]
}