export class Pupil {
    public pupil_id : number
    public passport : string 
    public surname : string
    public firstname : string
    public othernames : string 
    public dob : string 
    public gender : string 
    public parentname : string
    public phone : string 
    public email : string 
    public address : string 
    public height : number
    public weight : number
    public school : number 
    public fingerprint : string
    public vulnerability_status : string
    public class : string
    public head_teacher : string
    
}