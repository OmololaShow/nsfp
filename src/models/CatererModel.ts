export class CatererModel {
    public id : string
    public fullname : string
    public passport : string
    public fingerprint : string
    public dob : string 
    public bank_id : number
    public accno : string
    public accname : string
    public bvn : string
    public address : string
    public email : string
    public phone : string
    public employee_count : number 
    public pupilgroup : string 
    public pupilcount : number
    public maritalstatus : string 
    public state_id : number
    public senatorial_district_id : number
    public lga_id : number
    public ward_id : number
    public school_id : number
    public crop_farm_occupation : string
    public landtype : string 
    public landsize : number
	public linkedcooks_count : number
    public previousincome : number
    public currentincome  : number
    public aggregatordetails : string
}