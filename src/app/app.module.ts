import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Login } from '../pages/login/login';
import { Photo } from '../pages/photo/photo';
import { Listing } from '../pages/listing/listing';
import { Settings } from '../pages/settings/settings';
import { Register } from '../pages/register/register';
import { AddPupil } from '../pages/add-pupil/add-pupil';
import { CatererPage } from "../pages/caterer/caterer";
import { FarmerPage } from "../pages/farmer/farmer";
import { Contractors } from '../pages/contractors/contractors';
import { Camera } from '@ionic-native/camera';
import { DashboardPage } from "../pages/dashboard/dashboard";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { LgaProviderAll } from '../providers/lga-provider-all/lga-provider-all';
import { SchoolProviderAll } from '../providers/school-provider-all/school-provider-all';
import { SenatorialDistrictProviderAll } from '../providers/senatorial-district-provider-all/senatorial-district-provider-all';
import { WardProviderAll } from '../providers/ward-provider-all/ward-provider-all';
import { CatererProvider } from '../providers/caterer/caterer';
import { BankProvider } from '../providers/bank/bank';
import { FarmerProvider } from '../providers/farmer/farmer';
import { DashboardProvider } from '../providers/dashboard/dashboard';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Login,
    Photo,
    Listing,
    Settings,
    Register,
    AddPupil,
    Contractors,
    CatererPage,
    FarmerPage,
    DashboardPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Login,
    Photo,
    Listing,
    Settings,
    Register,
    AddPupil,
    Contractors,
    CatererPage,
    FarmerPage,
    DashboardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {
      provide: ErrorHandler, 
      useClass: IonicErrorHandler
    },
    LgaProviderAll,
    SchoolProviderAll,
    SenatorialDistrictProviderAll,
    WardProviderAll,
    CatererProvider,
    BankProvider,
    FarmerProvider,
    DashboardProvider
  ]
})
export class AppModule {}
