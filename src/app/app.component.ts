import { Component } from '@angular/core';
import { Platform, Events, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OfflineProvider } from "../providers/offline" ;
//import { TabsPage } from '../pages/tabs/tabs';
import { Login } from '../pages/login/login';
import { SenatorialDistrictProviderAll } from "../providers/senatorial-district-provider-all/senatorial-district-provider-all";
import { VulnerabilityAll } from "../providers/vulnerability-all/vulnerability-all";
import { WardProviderAll } from "../providers/ward-provider-all/ward-provider-all";
import { LgaProviderAll } from "../providers/lga-provider-all/lga-provider-all";
import { SchoolProviderAll } from "../providers/school-provider-all/school-provider-all";

//import { Photo } from '../pages/photo/photo';
//import { Listing } from '../pages/listing/listing';
//import { Settings } from '../pages/settings/settings';
//import { Register } from '../pages/register/register';
//import { AddPupil } from '../pages/add-pupil/add-pupil';
//import { Contractors } from '../pages/contractors/contractors';

@Component({
  templateUrl: 'app.html',
  providers : [OfflineProvider, VulnerabilityAll, SenatorialDistrictProviderAll, WardProviderAll, LgaProviderAll, SchoolProviderAll]
})
export class MyApp {
  rootPage:any = Login;

  constructor(platform: Platform,
      statusBar: StatusBar, 
      splashScreen: SplashScreen, 
      public offline : OfflineProvider,
      public senatorialDistrictAll : SenatorialDistrictProviderAll,
      public wardProviderAll : WardProviderAll,
      public lgaProviderAll : LgaProviderAll,
      public schoolProviderAll : SchoolProviderAll,
      public toastCtrl : ToastController,
      public vulnerabilityAll : VulnerabilityAll,
      public events : Events) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      

      this.schoolProviderAll.fetchAll().then((res : any) =>  {
          if(res != null) {
              let toast = this.toastCtrl.create({
                  message: 'School data sync successful',
                  duration: 2000
                });
            toast.present();
            localStorage.setItem("SchoolProviderAll", JSON.stringify(res.data));
          }
      });
      
      this.senatorialDistrictAll.fetchAll().then((res : any) => {
         if(res != null) {
              let toast = this.toastCtrl.create({
                  message: 'District data sync successful',
                  duration: 2000
                });
            toast.present();
          localStorage.setItem("SenatorialDistrictAll", JSON.stringify(res.data));
         }
      });

      this.wardProviderAll.fetchAll().then((res : any) =>  {
          if(res != null) {
                  let toast = this.toastCtrl.create({
                      message: 'Ward data sync successful',
                      duration: 2000
                    });
                toast.present();
                localStorage.setItem("WardProviderAll", JSON.stringify(res.data));
          }
      });

      this.lgaProviderAll.fetchAll().then((res : any) =>  {
         if(res != null) {
                  let toast = this.toastCtrl.create({
                      message: 'LGA data sync successful',
                      duration: 2000
                    });
                toast.present();
              localStorage.setItem("LGAProviderAll", JSON.stringify(res.data));
         }
      }); 

       
      this.vulnerabilityAll.fetchAll().then((res : any) =>  {
         if(res != null) {
                  let toast = this.toastCtrl.create({
                      message: 'Vulnerabilities data sync successful',
                      duration: 2000
                    });
                toast.present();
              localStorage.setItem("VulnerabilityList", JSON.stringify(res.data));
         }
      }); 

      setInterval((handler : any) => { 
         /** this.offline.load().then((data : any) => { 
            console.log("Fired!");
            this.events.publish("offline:status", true);
          }, (error : any) => {
            this.events.publish("offline:status", false);
          });**/
      }, 12000);
      
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
